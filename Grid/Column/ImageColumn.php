<?php

namespace FuncFunc\CommonBundle\Grid\Column;

use APY\DataGridBundle\Grid\Column\Column;

class ImageColumn extends Column
{
	protected $vichMappings;
	protected $fileNameField;
	protected $altTextField;
	protected $mapping;
	protected $imageFilter;

	public function __construct(array $vichMappings)
	{
		$this->vichMappings = $vichMappings;
	}

	public function getType()
	{
		return 'image';
	}

	public function getUriPrefix()
	{
		if($this->mapping)
		{
			return $this->vichMappings[$this->mapping]['uri_prefix'] . '/';
		}
		throw new \InvalidArgumentException(sprintf("The mapping field for image column '%s' wasn't set", $this->getId()));
	}

	public function getFileNameField()
	{
		return $this->fileNameField;
	}

	public function getAltTextField()
	{
		return $this->altTextField;
	}

	public function getImageFilter()
	{
		return $this->imageFilter;
	}

	public function __initialize(array $params)
	{
		parent::__initialize($params);

		$this->altTextField = $this->getParam('altTextField', false);
		$this->fileNameField = $this->getParam('fileNameField', $this->getId() . 'File');
		$this->mapping = $this->getParam('mapping');
		$this->imageFilter = $this->getParam('image_filter', false);

		$this->setFilterable(false);
		$this->setSortable(false);
	}
}
