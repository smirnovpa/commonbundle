<?php
namespace FuncFunc\CommonBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\Options;
use FuncFunc\CommonBundle\Extensions\Twig\ZendJsonExpr;

class ChoiceTypeExtension extends AbstractTypeExtension
{
    /**
     * {@inheritDoc}
     */
    public function getExtendedType()
    {
        return 'choice';
    }

    /**
     * {@inheritDoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setDefaults([
                'use_select2' => false,
                'empty_placeholder' => false,
                'hide_search_box' => false,
                'minimum_results_for_search' => 0,
                'extra_attributes' => false,
            ])
            ->setAllowedTypes([
                'use_select2' => 'bool',
                'empty_placeholder' => [
                    'bool',
                    'string',
                ],
                'hide_search_box' => 'bool',
                'minimum_results_for_search' => 'int',
                'extra_attributes' => [
                    'bool',
                    'array',
                ],
            ])
            ->setNormalizers([
                'empty_placeholder' => function(Options $options, $value)
                    {
                        return is_bool($value) ? false : $value;
                    },
                'minimum_results_for_search' => function(Options $options, $value)
                    {
                        return $options['hide_search_box'] || $value < 0 ? -1 : $value;
                    },
                'extra_attributes' => function(Options $options, $value)
                    {
                        return is_bool($value) ? false : $value;
                    },
            ])
        ;
    }

    /**
     * {@inheritDoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['use_select2'] = $options['use_select2'];
        $view->vars['empty_placeholder'] = $options['empty_placeholder'];
        $view->vars['minimum_results_for_search'] = $options['minimum_results_for_search'];
        $view->vars['extra_attributes'] = $options['extra_attributes'];
        $view->vars['placeholder_option'] = new ZendJsonExpr('function(select){return select.children("option[value=placeholder]").first();}');
    }
}
