<?php
namespace FuncFunc\CommonBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use FuncFunc\CommonBundle\EventListener\LocaleListener;

class LocaleDateTypeExtension extends AbstractTypeExtension
{
    private $pattern;
    private $locale;

    public function __construct(LocaleListener $listener)
    {
        $this->pattern = $listener->getDatePattern();
        $this->locale = $listener->getLocale();
    }

    protected function transformDatePattern($intlPattern)
    {
        return preg_replace(['/d/', '/y/', '/(?<!Y)Y(?!Y)/', '/\'/'], ['D', 'Y', 'YYYY', ''], $intlPattern);
//         return preg_replace(array('/(?<!y)y(?!y)/', '/(?<!m)mmm(?!m)/', '/mmmm/'), array('YYYY', 'M', 'MM'), strtolower($intlPattern));
    }

    protected function transformDatePatternToPhp($intlPattern)
    {
        $patterns = [
            '/(?<!y)y{1}(?!y)/' => 'Y',
            '/(?<!y)y{4}(?!y)/' => 'Y',
            '/(?<!y)y{2}(?!y)/' => 'y',
            '/(?<!M)M{1}(?!M)/' => 'n',
            '/(?<!M)M{2}(?!M)/' => 'm',
            '/(?<!M)M{3}(?!M)/' => 'M',
            '/(?<!M)M{4}(?!M)/' => 'F',
            '/(?<!d)d{1}(?!d)/' => 'j',
            '/(?<!d)d{2}(?!d)/' => 'd',
            '/(?<!E)E{1,3}(?!E)/' => 'D',
            '/(?<!E)E{4}(?!E)/' => 'l',
        ];
        return preg_replace(array_keys($patterns), array_values($patterns), $intlPattern);
    }

    /**
     * {@inheritDoc}
     */
    public function getExtendedType()
    {
        return 'date';
    }

    /**
     * {@inheritDoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setDefaults([
                'format' => DateType::DEFAULT_FORMAT,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        if('single_text' === $options['widget'])
        {
            $view->vars['intl_pattern'] = is_string($options['format']) && $options['format'] ? $options['format'] : $this->pattern;
            $view->vars['template_pattern'] = $this->transformDatePattern(is_string($options['format']) && $options['format'] ? $options['format'] : $this->pattern);
            $view->vars['php_pattern'] = $this->transformDatePatternToPhp(is_string($options['format']) && $options['format'] ? $options['format'] : $this->pattern);
            $view->vars['locale'] = $this->locale;
        }
    }
}