<?php
namespace FuncFunc\CommonBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\Options;
use FuncFunc\CommonBundle\EventListener\LocaleListener;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;

class LocaleDateTimeTypeExtension extends AbstractTypeExtension
{
    private $pattern;
    private $locale;

    public function __construct(LocaleListener $listener)
    {
        $this->pattern = $listener->getDateTimePattern();
        $this->locale = $listener->getLocale();
    }

    protected function transformDateTimePattern($intlPattern)
    {
        return preg_replace(['/d/', '/y/', '/(?<!Y)Y(?!Y)/', '/\'/', '/Z+/'], ['D', 'Y', 'YYYY', '', 'Z'], $intlPattern);
    }

    /**
     * {@inheritDoc}
     */
    public function getExtendedType()
    {
        return 'datetime';
    }

    /**
     * {@inheritDoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $pattern = $this->pattern;
        $resolver
            ->setDefaults([
                'minute_step' => 1,
                'time_format' => null
            ])
            ->replaceDefaults([
                'format' => $this->pattern,
            ])
            ->setAllowedTypes([
                'minute_step' => 'integer',
            ])
            ->setNormalizers([
                'with_minutes' => function(Options $options, $value) use ($pattern)
                    {
                        return $options['widget'] == 'single_text' ? strpos($options['format'], 'm') > 0 : $value;
                    },
                'with_seconds' => function(Options $options, $value) use ($pattern)
                    {
                        return $options['widget'] == 'single_text' ? strpos($options['format'], 's') > 0 : $value;
                    },
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        if('single_text' === $options['widget'])
        {
            $view->vars['locale'] = $this->locale;
            $view->vars['minute_step'] = $options['minute_step'];
            $view->vars['intl_pattern'] = is_string($options['format']) && $options['format'] ? $options['format'] : $this->pattern;
            $view->vars['template_pattern'] = $this->transformDateTimePattern(is_string($options['format']) && $options['format'] ? $options['format'] : $this->pattern);
            $view->vars['with_minutes'] = $options['with_minutes'];
            $view->vars['with_seconds'] = $options['with_seconds'];
        }
    }

    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if('single_text' !== $options['widget'] && !is_null($options['time_format']))
        {
            $timeOptions = array_intersect_key(
                $builder->get('time')->getOptions(),
                array_flip(array(
                    'hours',
                    'minutes',
                    'seconds',
                    'with_minutes',
                    'with_seconds',
                    'empty_value',
                    'required',
                    'translation_domain',
                    'input',
                    'error_bubbling',
                    'widget',
                ))
            );
            $builder->add('time', 'time', array_merge($timeOptions, ['format' => $options['time_format']]));
        }
    }
}
