<?php
namespace FuncFunc\CommonBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\Exception\MissingOptionsException;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Vich\UploaderBundle\Mapping\PropertyMappingFactory;

class FileTypeExtension extends AbstractTypeExtension
{
    const FILE_TYPE_NONE = 'file_type_none';
    const FILE_TYPE_DOWNLOADABLE = 'file_type_downloadable';
    const FILE_TYPE_IMAGE = 'file_type_image';

    private $propertyAccessor;
    private $mappingFactory;

    public function __construct(PropertyAccessorInterface $propertyAccessor, PropertyMappingFactory $mappingFactory = null)
    {
        $this->mappingFactory = $mappingFactory;
        $this->propertyAccessor = $propertyAccessor;
    }

    /**
     * {@inheritDoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if(($view->vars['parent_object'] = $form->getParent() ? $form->getParent()->getData() : null) &&
            $options['file_name_property'] &&
            $this->propertyAccessor->getValue($view->vars['parent_object'], $options['file_name_property']) &&
            !$options['always_required'])
        {
            $view->vars['required'] = false;
        }
        $view->vars['file_type'] = $options['file_type'];
        $view->vars['file_name_property'] = $options['file_name_property'];
        $view->vars['image_filter'] = $options['image_filter'];
        $view->vars['vich_mapping'] = !is_null($view->vars['parent_object']) ?
            $this->mappingFactory->fromField($view->vars['parent_object'], $view->vars['name'])->getMappingName() :
            null
        ;
    }

    /**
     * {@inheritDoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setDefaults([
                'file_type' => self::FILE_TYPE_NONE,
                'file_name_property' => false,
                'image_filter' => false,
                'always_required' => false,
            ])
            ->setAllowedValues([
                'file_type' => [
                    self::FILE_TYPE_DOWNLOADABLE,
                    self::FILE_TYPE_IMAGE,
                    self::FILE_TYPE_NONE,
                ],
            ])
            ->setAllowedTypes([
                'file_name_property' => [
                    'bool',
                    'string',
                ],
                'image_filter' => [
                    'bool',
                    'string',
                ],
                'always_required' => 'bool',
            ])
            ->setNormalizers([
                'file_type' => function(Options $options, $value)
                    {
                        switch($value)
                        {
                            case self::FILE_TYPE_IMAGE:
                            case self::FILE_TYPE_DOWNLOADABLE:
                                if($options['file_name_property'] === false)
                                {
                                    throw new MissingOptionsException(sprintf(
                                        'You should set "%s" option when set "%s" option to "%s"',
                                        'file_name_property',
                                        'file_type',
                                        strtoupper($value)
                                    ));
                                }
                        }
                        return $value;
                    },
                'file_name_property' => function(Options $options, $value)
                    {
                        return is_bool($value) ? false : $value;
                    },
                'image_filter' => function(Options $options, $value)
                    {
                        return is_bool($value) ? false : $value;
                    },
            ])
        ;
    }

    /**
     * {@inheritDoc}
     */
    public function getExtendedType()
    {
        return 'file';
    }
}