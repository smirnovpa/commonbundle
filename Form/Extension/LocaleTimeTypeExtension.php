<?php
namespace FuncFunc\CommonBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use FuncFunc\CommonBundle\EventListener\LocaleListener;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Exception\InvalidConfigurationException;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\Options;

class LocaleTimeTypeExtension extends AbstractTypeExtension
{
    private $pattern;
    private $locale;

    public function __construct(LocaleListener $listener)
    {
        $this->pattern = $listener->getTimePattern();
        $this->locale = $listener->getLocale();
    }

    /**
     * {@inheritDoc}
     */
    public function getExtendedType()
    {
        return 'time';
    }

    /**
     * {@inheritDoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $pattern = $this->pattern;
        $resolver
            ->setDefaults([
                'minute_step' => 15,
                'format' => false,
            ])
            ->setAllowedTypes([
                'minute_step' => 'integer',
                'format' => [
                    'bool',
                    'string',
                ],
            ])
            ->setNormalizers([
                'format' => function(Options $options, $value) use ($pattern)
                    {
                        if(is_bool($value))
                        {
                            return $pattern;
                        }
                        return $value;
                    },
                'with_minutes' => function(Options $options, $value) use ($pattern)
                    {
                        return $options['widget'] == 'single_text' ? strpos($options['format'], 'm') > 0 : $value;
                    },
                'with_seconds' => function(Options $options, $value) use ($pattern)
                    {
                        return $options['widget'] == 'single_text' ? strpos($options['format'], 's') > 0 : $value;
                    },
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if('single_text' === $options['widget'])
        {
            $format = str_replace(['mm', 'm', 'ss', 'HH', 'hh'], ['i', 'i', 's', 'H', 'h'], $options['format']);
            $builder
                ->resetViewTransformers()
                ->addViewTransformer(new DateTimeToStringTransformer($options['model_timezone'], $options['view_timezone'], $format))
            ;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if('single_text' === $options['widget'])
        {
            $view->vars['locale'] = $this->locale;
            $view->vars['template_pattern'] = is_string($options['format']) && $options['format'] ? $options['format'] : $this->pattern;
            $view->vars['minute_step'] = $options['minute_step'];
        }
    }
}