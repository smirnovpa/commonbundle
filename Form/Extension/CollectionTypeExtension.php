<?php
namespace FuncFunc\CommonBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;

class CollectionTypeExtension extends AbstractTypeExtension
{
    /**
     * {@inheritDoc}
     */
    public function getExtendedType()
    {
        return 'collection';
    }

    /**
     * {@inheritDoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setDefaults([
                'show_add_button' => true,
                'add_button_label' => 'Add Record',
                'no_data_message' => 'No records yet',
                'add_button_icon' => 'plus',
            ])
            ->setAllowedTypes([
                'show_add_button' => 'bool',
                'add_button_label' => 'string',
                'no_data_message' => 'string',
                'add_button_icon' => 'string',
            ])
        ;
    }

    /**
     * {@inheritDoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['show_add_button'] = (int)$options['show_add_button'];
        $view->vars['add_button_label'] = $options['add_button_label'];
        $view->vars['no_data_message'] = $options['no_data_message'];
        $view->vars['add_button_icon'] = $options['add_button_icon'];
        $view->vars['add_button_class'] = "js-{$view->vars['id']}_add_record";
        $view->vars['prototype_name'] = $options['prototype_name'];
    }

    /**
     * {@inheritDoc}
     */
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['next_child_index'] = count($view->children) === 0 ? 0 : (max(array_keys($view->children)) + 1);
    }
}