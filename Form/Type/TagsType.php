<?php
namespace FuncFunc\CommonBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Extension\Core\EventListener\ResizeFormListener as ResizeFormListenerOriginal;
use FuncFunc\CommonBundle\Form\DataTransformer\TagTransformer;
use Doctrine\ORM\EntityManager;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\Options;

class TagsType extends AbstractType
{
    private $em;
    private $properyAccessor;

    public function __construct(EntityManager $em, PropertyAccessorInterface $propertyAccessor)
    {
        $this->em = $em;
        $this->properyAccessor = $propertyAccessor;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->addModelTransformer(new TagTransformer($this->em, $this->properyAccessor, $options));
    }

    /**
     * {@inheritDoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $propertyAccessor = $this->properyAccessor;
        $resolver
            ->setDefaults([
                'allow_new_tags' => false,
                'tag_label' => 'name',
                'empty_placeholder' => 'Enter tag here',
                'tag_search_router' => '',
                'tag_search_router_params' => [],
                'tag_search_param' => 'tag',
                'existing_tag_list' => [],
                'required' => false,
                'tag_to_string_transformer' => null,
                'minimum_input_length' => 3,
            ])
            ->setRequired(['tag_data_class'])
            ->setAllowedTypes([
                'allow_new_tags' => 'bool',
                'tag_data_class' => 'string',
                'tag_label' => 'string',
                'tag_search_router' => 'string',
                'tag_search_router_params' => 'array',
                'tag_search_param' => 'string',
                'existing_tag_list' => 'array',
                'tag_to_string_transformer' => 'callable',
                'minimum_input_length' => 'int',
            ])
            ->setNormalizers([
                'tag_to_string_transformer' => function(Options $options, $value) use ($propertyAccessor)
                {
                    if(is_null($value))
                    {
                        $value = function($tag) use ($propertyAccessor, $options)
                        {
                            return $propertyAccessor->getValue($tag, $options['tag_label']);
                        };
                    }
                    return $value;
                },
                'existing_tag_list' => function(Options $options, $value)
                {
                    return array_map(
                        function($element) use ($options)
                        {
                            $element = $options['tag_to_string_transformer']($element);
                            return [
                                'id' => str_replace(',', '&#44;', $element),
                                'text' => $element,
                            ];
                        },
                        $value
                    );
                },
            ])
        ;
    }

    /**
     * {@inheritDoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['allow_new_tags'] = $options['allow_new_tags'];
        $view->vars['tag_search_router'] = $options['tag_search_router'];
        $view->vars['tag_search_router_params'] = $options['tag_search_router_params'];
        $view->vars['tag_search_param'] = $options['tag_search_param'];
        $view->vars['empty_placeholder'] = $options['empty_placeholder'];
        $view->vars['existing_tag_list'] = $options['existing_tag_list'];
        $view->vars['tag_label'] = $options['tag_label'];
        $view->vars['tag_to_string_transformer'] = $options['tag_to_string_transformer'];
        $view->vars['minimum_input_length'] = $options['minimum_input_length'];
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'tags';
    }

    /**
     * {@inheritDoc}
     */
    public function getParent()
    {
        return 'text';
    }
}