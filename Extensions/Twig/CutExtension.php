<?php
namespace FuncFunc\CommonBundle\Extensions\Twig;

class CutExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            'cut' => new \Twig_Filter_Method($this, 'filterCut', array('length' => false, $wordCut = false, 'appendix' => false)),
        );
    }

    /**
     * @param string $text
     * @param integer $length
     * @param boolean $wordCut
     * @param string $appendix
     * @return string
     */
    public function filterCut($text, $length = 160, $wordCut = true, $appendix = '...')
    {
        $maxLength = (int)$length - mb_strlen($appendix,'UTF-8');
        if (mb_strlen($text, 'UTF-8') > $maxLength) {
            if($wordCut){
                $text = mb_substr($text, 0, $maxLength + 1,'UTF-8');
                $text = mb_substr($text, 0, mb_strrpos($text, ' ','UTF-8'),'UTF-8');
            }
            else {
                $text = mb_substr($text, 0, $maxLength,'UTF-8');
            }
            $text .= $appendix;
        }

        return $text;
    }

    public function getName()
    {
        return 'funcfunc.common_bundle.twig.cut_extension';
    }
}