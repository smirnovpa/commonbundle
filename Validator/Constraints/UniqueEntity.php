<?php
namespace FuncFunc\CommonBundle\Validator\Constraints;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity as UE;

/**
 * @Annotation
 */
class UniqueEntity extends UE
{
    public $ignoreFalseLikeValues = false;

    /**
     * {@inheritDoc}
     */
    public function validatedBy()
    {
        return 'funcfunc.common_bundle.validator.unique_entity';
    }
}