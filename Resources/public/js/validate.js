var ValidateForms = function(){
	var formValidator = function(){
		$("form.form-horizontal:visible").validate();
	}
	return { init: init, validationRules: getValidationRules() };

	function init () {
		jQuery.validator.addMethod('date', function(){return true;});//We don't use date validation since we control this via datetimepicker
		jQuery.validator.setDefaults(getValidationRules());
		$('select.select2-offscreen, input[type=hidden].select2-offscreen').on('change', function(){
			$(this).valid();
		});
		formValidator();
	}

	function getValidationRules() {
		return {
			focusCleanup: false,
			wrapper: '',
			ignore: 'input[type=hidden]:not(.select2-offscreen)',
			errorElement: 'span',
			errorClass: 'label label-danger',
			highlight: function(element) {},
			success: function(element) {
				var group = $(element).closest('.form-group-validation')
					.removeClass('has-error')
					.removeClass('has-feedback')
				;
				$('span.glyphicon, span.label', group).remove();
			},
			errorPlacement: function(error, element) {
				var $formGroup = element.closest('.form-group-validation');
				$formGroup
					.addClass('has-error')
					.addClass('has-feedback')
				;
				if(!element.hasClass('form-control'))
				{
					element = element.closest('.form-control');
				}
				else if(element.parent().hasClass('input-group'))
				{
					element = element.parent();
				}
				error.insertAfter(element);
				element.after($('<span>', {'class': 'glyphicon glyphicon-warning-sign form-control-feedback'}));
			},
			submitHandler: function(form) {
				var $form = $(form);
				if($form.data("submitted") === undefined)
				{
					$form.data("submitted", "1");
					form.submit();
				}
			},
			invalidHandler: function(event, validator) {
				var $firstErrorElement = $(validator.errorList[0].element),
					$tabsContents = $firstErrorElement.parents('.tab-pane'),
					$tabsButtonsTarget = $('.nav a[data-target]'),
					$tabsButtonsHref = $('.nav a[href^=#][href!=#]');
				$tabsContents.each(function(){
					var $tabContent = $(this),
						found = false;
					$tabsButtonsTarget.each(function(){
						var $tabButton = $(this);
						if($tabContent.is($tabButton.data('target')))
						{
							$tabButton.tab('show');
							found = true;
							return false;
						}
					});
					if(!found)
					{
						$tabsButtonsHref.each(function(){
							var $tabButton = $(this);
							if($tabContent.is($tabButton.attr('href')))
							{
								$tabButton.tab('show');
								return false;
							}
						});
					}
				});
			}
		};
	}
}();
